#!/bin/bash

set -u

GREEN="\e[0;32m"
YELLOW="\e[0;33m"
RED="\e[0;31m"
END="\e[0m"

usage () {
	cat <<EOF
Usage:
	run-test.sh [-h|--help] [-a <arg>|--arg <arg>] <command> [<args>]

Run various commands to test bst.

Commands:

	test	Run the test suite. If no arguments are given, the full
                suite is run, otherwise the given arguments will be run
	run	Run the test suite.  (Does not clean)
	clean	Clean temporary test files
	omit	Omit the given test from test runs
	include	Include the given test in test runs

Options:
	--help	Display this help message and exit
	--arg	Specify an argument for bst, such as --colors
EOF
}

main () {
	while : ;
	do
		case "${1:-}" in
			"test")
				shift
				clean "$@"
				run "$@"
				break ;;
			"run")
				shift
				run "$@"
				break ;;
			"clean")
				shift
				clean "$@"
				break ;;
			"omit")
				shift
				omit "$@"
				break ;;
			"include")
				shift
				include "$@"
				break ;;
			-a|--arg)
				export BST_FLAGS="${BST_FLAGS:-} $2"
				shift 2 ;;
			-h|--help)
				usage
				break ;;
			*)
				echo "Error: Unrecognized argument '${1:-}'" 1>&2
				usage
				break ;;
		esac
	done
}

# run
#
# Run all tests in the current directory.
run () {
	local succeeded=0
	local omitted=0
	local failed=0
	local state
	local tests
	local dir

	if [ $# -ge 1 ];
	then
		tests=$@
	else
		tests="*"
	fi

	for dir in $tests;
	do
		if [ -d "$dir" ]
		then
			run-test "$dir"
			state=$?
			if [ $state == 0 ]
			then
				((succeeded++))
			elif [ $state == 2 ]
			then
				((omitted++))
			else
				((failed++))
			fi
		fi
	done

	echo
	printf "%4s test%.*s ${GREEN}succeeded${END}.\n" $succeeded $((succeeded != 1)) "s"
	printf "%4s test%.*s ${YELLOW}omitted${END}.\n" $omitted $((omitted != 1)) "s"
	printf "%4s test%.*s ${RED}failed${END}.\n" $failed $((failed != 1)) "s"

	if [ $failed != 0 ]
	then
	   exit 1
	fi
}

# clean
#
# Clean all tests in the current directory.
clean () {
	local dir

	for dir in *;
	do
		if [ -d "$dir" ]
		then
			(cd "$dir" || exit 1
			 rm -rf "results/"*)
		fi
	done
}

# run-test
#
# Run the test in the given directory
#
# Args:
#    test ($1) - The test to run
#
run-test () {
	local test="$1"

	touch .omit
	if grep -q "$test" .omit
	then
		echo -e "${YELLOW}Omitting${END} test $test."
		return 2
	fi

	echo "Running tests for test case '$test'"

	(cd "$test" || exit 1
	 bash "run-$(basename "$test").sh")

	if [ ! "$?" -eq 0 ]
	then
		echo -e "Tests for '$test' ${RED}failed${END}.\n" 2>&1
		return 1
	fi
}

# omit
#
# Ignore the given test during future test runs
#
# Args:
#    test ($1) - The test to ignore
#
omit() {
	local test="$1"

	# Tell the user if we don't need to omit the file
	touch .omit
	if grep -q "$test" .omit
	then
		echo "Test $test is already omitted." 2>&1
		exit 1
	fi

	if [ -d "$test" ]
	then
		echo "$test" >> .omit
	else
		echo "No such test." 2>&1
		exit 1
	fi
}

# include
#
# After a test has been omitted, re-include it
#
# Args:
#    test ($1) - The test to include
#
include() {
	local test="$1"

	local temp

	touch .omit

	# Make a temporary file to inverse grep to
	temp=$(mktemp)

	# Tell the user if we don't need to include the file
	if ! grep -q "$test" .omit
	then
		echo "Test $test is already included." 2>&1
		exit 1
	fi

	# Remove the line containing the test string
	grep -v "$test" .omit > "$temp"; mv "$temp" .omit
}

main "$@"
